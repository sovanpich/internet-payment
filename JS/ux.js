const timing = new Date();
var hs = timing.getHours();
var ms = timing.getMinutes();
var he;
var me;


function start() {
    if (hs >= 0 && hs < 10) {
        document.getElementById('timeStart').innerHTML = "0" + hs + " :" + ms + " AM";
        document.getElementById('btbStart').style.display = "none";
        document.getElementById('btbStop').style.display = "block";
    } else if (ms >= 0 && ms < 10) {
        document.getElementById('timeStart').innerHTML = hs + " :0" + ms + " AM";
        document.getElementById('btbStart').style.display = "none";
        document.getElementById('btbStop').style.display = "block";
    } else if (hs >= 0 && hs < 12) {
        document.getElementById('timeStart').innerHTML = hs + " :" + ms + " AM";
        document.getElementById('btbStart').style.display = "none";
        document.getElementById('btbStop').style.display = "block";
    } else {
        document.getElementById('timeStart').innerHTML = hs + " :" + ms + " PM";
        document.getElementById('btbStart').style.display = "none";
        document.getElementById('btbStop').style.display = "block";
    }

}



function stop() {
    var timend = new Date();
    he = timend.getHours();
    me = timend.getMinutes();
    if (he >= 0 && he < 10) {
        document.getElementById('timeStop').innerHTML = "0" + he + ": " + me + " AM ";

        document.getElementById('btbStop').style.display = "none";
        document.getElementById('btbClear').style.display = "block";
    } else if (me >= 0 && me < 10) {
        document.getElementById('timeStop').innerHTML = he + " :0" + me + " AM";

        document.getElementById('btbStop').style.display = "none";
        document.getElementById('btbClear').style.display = "block";
    } else if (he >= 0 && he < 12) {
        document.getElementById('timeStop').innerHTML = he + " :" + me + " AM";

        document.getElementById('btbStop').style.display = "none";
        document.getElementById('btbClear').style.display = "block";
    } else {
        document.getElementById('timeStop').innerHTML = he + " :" + me + " PM";
        document.getElementById('btbStart').style.display = "none";
        document.getElementById('btbStop').style.display = "none";
        document.getElementById('btbClear').style.display = "block";
    }
    var totalh = (he - hs) * 60;
    var totalm = me - ms;
    var total = totalh + totalm
    var reil = 500;
    var mn = 15;
    document.getElementById('totalMin').innerHTML = total;
    if (total >= 0 && total <= mn) {
        document.getElementById('money').innerHTML = reil;
    } else if (total > mn && total <= 2 * mn) {
        document.getElementById('money').innerHTML = 2 * reil;
    } else if (total > 2 * mn && total <= 3 * mn) {
        document.getElementById('money').innerHTML = 3 * reil;
    } else if (total > 3 * mn && total <= 4 * mn) {
        document.getElementById('money').innerHTML = 4 * reil;
    } else if (total > 4 * mn && total <= 5 * mn) {
        document.getElementById('money').innerHTML = 5 * reil;
    } else if (total > 5 * mn && total <= 6 * mn) {
        document.getElementById('money').innerHTML = 6 * reil;
    } else if (total > 6 * mn && total <= 7 * mn) {
        document.getElementById('money').innerHTML = 7 * reil;
    }
}

function btbClear() {
    document.getElementById('timeStart').innerHTML = "0:00";
    document.getElementById('timeStop').innerHTML = "0:00";
    document.getElementById('totalMin').innerHTML = "0";
    document.getElementById('money').innerHTML = "0";
    document.getElementById('btbStart').style.display = "block";
    document.getElementById('btbClear').style.display = "none";

}